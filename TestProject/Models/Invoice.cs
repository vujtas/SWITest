﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestProject.Models
{
    public class Invoice
    {
        IEnumerable<OrderItem> _orders;
        public Invoice(IEnumerable<OrderItem> orders)
        {
            _orders = orders;
        }

        public void ProcessInvoice()
        {
            CheckAllOrders();
            CreateInvoice("faktura.pdf");
            CloseCreatedInvoice();
        }

        /// <summary>
        /// zkontroluje všechny objednávky a kdyžtak vyhodí chybu
        /// </summary>
        private void CheckAllOrders()
        {
            foreach (var order in _orders)
            {
                //todo
            }
        }
        /// <summary>
        /// metoda vytvoří fakturu a uloží ji na danou cestu
        /// </summary>
        /// <param name="path">daná cesta k souboru</param>
        /// <returns></returns>
        private string CreateInvoice(string path)
        {
            throw new Exception("Not created");
        }

        /// <summary>
        /// uzavře účetně vytvořenou fakturu
        /// </summary>
        private void CloseCreatedInvoice()
        {
            throw new Exception("Not created");
        }

    }
}