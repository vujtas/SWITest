﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestProject.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        public double TotalPrice 
        { 
            get {
                double itemSum = 0;
                if (OrderItem == null || OrderItem.Count == 0)
                    return 0;
                else
                    foreach(var item in OrderItem)
                    {
                        itemSum += item.Price;
                    }
                return itemSum;
            } 
        }
        public List<OrderItem> OrderItem { get; set; }
        public Customer Customer { get; set; }
    }
}