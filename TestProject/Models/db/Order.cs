﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestProject.Models.db
{
    public static class Order
    {
        /// <summary>
        /// Metoda vrátí pouze objednávky od daného uživatele
        /// </summary>
        /// <param name="supplierId">id uživatele</param>
        /// <returns></returns>
        public static List<Models.Order> GetOrdersBySupplier(int supplierId)
        {
            return new List<Models.Order>();
        }

        /// <summary>
        /// Metoda vrátí všechny Objednávky, které má systém uložené
        /// </summary>
        /// <returns></returns>
        public static List<Models.Order> GetAllOrders()
        {
            return new List<Models.Order>();
        }





    }
}