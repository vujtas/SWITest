﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestProject.Models.db
{
    public static class Customer
    {

        /// <summary>
        /// Vrátí dodavatele podle jeho idčka
        /// </summary>
        /// <param name="supplierId">id dodavatele</param>
        /// <returns></returns>
        public static Models.Customer GetCustomerById(int customerid)
        {
            return new Models.Customer();
        }

        /// <summary>
        /// Vrátí všechny možné dodavatele
        /// </summary>
        /// <returns></returns>
        public static List<Models.Customer> GetAllCustomers()
        {
            return new List<Models.Customer>();
        }




    }
}