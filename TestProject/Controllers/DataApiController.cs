﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Http.Results;

namespace TestProject.Controllers
{
    public class DataApiController : ApiController
    {

        [System.Web.Http.Route("api/GetCustomer/{customerId}")]
        public System.Web.Http.Results.JsonResult<Models.Customer> GetCustomerById(int customerId)
        {
            Models.Customer suppliers = Models.db.Customer.GetCustomerById(customerId);
            return Json(suppliers);
        }

        [System.Web.Http.Route("api/GetCustomers")]
       public System.Web.Http.Results.JsonResult<List<Models.Customer>> GetAllCustomers()
        {
            List<Models.Customer> suppliers = Models.db.Customer.GetAllCustomers();
            return Json(suppliers);
        }

    }
}