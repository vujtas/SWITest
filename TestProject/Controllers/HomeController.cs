﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestProject.Controllers
{
    public class HomeController : Controller
    {
        [Route("supplier/{supplierId}/orders")]
        public ActionResult SupplierOrders(int supplierId)
        {
            List<Models.Order> orders = Models.db.Order.GetOrdersBySupplier(supplierId);
            return View(orders);
        }

        public ActionResult Index()
        {
            List<Models.Order> orders = Models.db.Order.GetAllOrders();
            return View(orders);
        }





    }
}